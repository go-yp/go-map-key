package main

import "testing"

func BenchmarkArrayInterfaceKey(b *testing.B) {
	type UserGroupKey [2]interface{}

	var data = make(map[UserGroupKey]int, b.N)

	for i := 0; i < b.N; i++ {
		data[UserGroupKey{uint32(i), uint32(i)}] = i
	}
}

func BenchmarkArrayIntKey(b *testing.B) {
	type UserGroupKey [2]uint32

	var data = make(map[UserGroupKey]int, b.N)

	for i := 0; i < b.N; i++ {
		data[UserGroupKey{uint32(i), uint32(i)}] = i
	}
}

func BenchmarkStructIntKey(b *testing.B) {
	type UserGroupKey struct {
		Shard    uint32
		Timezone uint32
	}

	var data = make(map[UserGroupKey]int, b.N)

	for i := 0; i < b.N; i++ {
		data[UserGroupKey{uint32(i), uint32(i)}] = i
	}
}

func BenchmarkNestedIntMapKey(b *testing.B) {
	var data = make(map[uint32]map[uint32]int, 1024)

	for i := 0; i < b.N; i++ {
		var nestedKey = uint32(i & 1023)

		if nestedData, ok := data[nestedKey]; ok {
			nestedData[uint32(i)] = i
		} else {
			data[nestedKey] = map[uint32]int{
				uint32(i): i,
			}
		}
	}
}

func BenchmarkIntShiftKey(b *testing.B) {
	var data = make(map[uint64]int, b.N)

	for i := 0; i < b.N; i++ {
		data[uint64(i<<32)|uint64(i)] = i
	}
}
