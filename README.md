# Go map key benchmark

# Results
```bash
go test ./... -v -bench=. -benchmem
```

```text
BenchmarkArrayInterfaceKey        155 ns/op	     108 B/op	       2 allocs/op
BenchmarkArrayIntKey              104 ns/op	      28 B/op	       0 allocs/op
BenchmarkStructIntKey             102 ns/op	      28 B/op	       0 allocs/op
BenchmarkNestedIntMapKey          181 ns/op	      70 B/op	       0 allocs/op
BenchmarkIntShiftKey               69.7 ns/op	      32 B/op	       0 allocs/op
BenchmarkNamespaceSetKeyBinString 198 ns/op	      60 B/op	       1 allocs/op
BenchmarkNamespaceSetKeyBinStruct 116 ns/op	      32 B/op	       0 allocs/op
```