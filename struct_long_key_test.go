package main

import (
	"strconv"
	"testing"
)

const (
	namespaceSetKeyBinSize = 10*4 + 3 // 10 is uint32 size, 3 delimiters '_'
)

func BenchmarkNamespaceSetKeyBinStringWrite(b *testing.B) {
	var data = make(map[string]bool, b.N)

	// only odd
	for i := 0; i < b.N; i++ {
		data[namespaceSetKeyBinString(uint32(i), uint32(i), uint32(i), uint32(i))] = true
	}
}

func BenchmarkNamespaceSetKeyBinStringRead(b *testing.B) {
	var data = make(map[string]bool, b.N)

	// only odd
	for i := 0; i < b.N; i++ {
		data[namespaceSetKeyBinString(uint32(i), uint32(i), uint32(i), uint32(i))] = true
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = data[namespaceSetKeyBinString(uint32(i), uint32(i), uint32(i), uint32(i))]
	}
}

func BenchmarkNamespaceSetKeyBinStructWrite(b *testing.B) {
	type namespaceSetKeyBin struct {
		namespace, set, key, bin uint32
	}

	var data = make(map[namespaceSetKeyBin]bool, b.N)

	// only odd
	for i := 0; i < b.N; i++ {
		data[namespaceSetKeyBin{uint32(i), uint32(i), uint32(i), uint32(i)}] = true
	}
}

func BenchmarkNamespaceSetKeyBinStructRead(b *testing.B) {
	type namespaceSetKeyBin struct {
		namespace, set, key, bin uint32
	}

	var data = make(map[namespaceSetKeyBin]bool, b.N)

	// only odd
	for i := 0; i < b.N; i++ {
		data[namespaceSetKeyBin{uint32(i), uint32(i), uint32(i), uint32(i)}] = true
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = data[namespaceSetKeyBin{uint32(i), uint32(i), uint32(i), uint32(i)}]
	}
}

func namespaceSetKeyBinString(namespace, set, key, bin uint32) string {
	result := make([]byte, 0, namespaceSetKeyBinSize)

	result = strconv.AppendUint(result, uint64(namespace), 10)
	result = append(result, '_')
	result = strconv.AppendUint(result, uint64(set), 10)
	result = append(result, '_')
	result = strconv.AppendUint(result, uint64(key), 10)
	result = append(result, '_')
	result = strconv.AppendUint(result, uint64(bin), 10)

	return string(result)
}
