package example_before

import (
	"sync"
)

type Request interface {
	Namespace() uint64
	Set() uint64
}

type FullPathValue struct {
	Namespace uint64
	Set       uint64
}

var (
	namespaceSetExistsMap map[FullPathValue]bool
	namespaceSetMutex     sync.RWMutex
)

// handle user request
func UpdateHandler(request Request) {
	if namespaceSetExists(request.Namespace(), request.Set()) {
		return
	}

	// some action
}

// update by cron
func updateNamespaceSet(values []FullPathValue) {
	var replace = make(map[FullPathValue]bool, len(values))

	for _, value := range values {
		replace[value] = true
	}

	namespaceSetMutex.Lock()
	namespaceSetExistsMap = replace
	namespaceSetMutex.Unlock()
}

func namespaceSetExists(namespace, set uint64) bool {
	namespaceSetMutex.RLock()
	var data = namespaceSetExistsMap
	namespaceSetMutex.RUnlock()

	return data[FullPathValue{namespace, set}]
}
