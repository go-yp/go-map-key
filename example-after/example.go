package example_before

import (
	"strconv"
	"sync"
)

type Request interface {
	Namespace() uint64
	Set() uint64
}

type FullPathValue struct {
	Namespace uint64
	Set       uint64
}

var (
	namespaceSetExistsMap map[string]bool
	namespaceSetMutex     sync.RWMutex
)

// handle user request
func UpdateHandler(request Request) {
	if namespaceSetExists(request.Namespace(), request.Set()) {
		return
	}

	// some action
}

// update by cron
func updateNamespaceSet(values []FullPathValue) {
	var replace = make(map[string]bool, len(values))

	for _, value := range values {
		replace[namespaceSetString(value.Namespace, value.Set)] = true
	}

	namespaceSetMutex.Lock()
	namespaceSetExistsMap = replace
	namespaceSetMutex.Unlock()
}

func namespaceSetExists(namespace, set uint64) bool {
	namespaceSetMutex.RLock()
	var data = namespaceSetExistsMap
	namespaceSetMutex.RUnlock()

	return data[namespaceSetString(namespace, set)]
}

func namespaceSetString(namespace, set uint64) string {
	const (
		namespaceSetSize = 21*2 + 1 // 21 is uint64 size, 1 delimiters '_'
	)

	result := make([]byte, 0, namespaceSetSize)

	result = strconv.AppendUint(result, uint64(namespace), 10)
	result = append(result, '_')
	result = strconv.AppendUint(result, uint64(set), 10)

	return string(result)
}
